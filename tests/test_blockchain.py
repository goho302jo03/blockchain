import os

import pytest
from concurrent.futures import ThreadPoolExecutor
from decorator import decorator
from threading import Thread

from libs.client import sign_transaction
from libs.blockchain import Transaction, Block, BlockChain

@decorator
def handle_not_implemented_exception(f, *args, **kwargs):
    try:
        return f(*args, **kwargs)
    except NotImplementedError as e:
        pytest.skip('Not Implemented')
    return f(*args, **kwargs)


@pytest.fixture()
def block():
    return Block('previous_hash', 5, 'cheese', 50, 1631072278)


@pytest.fixture()
def blockchain():
    with ThreadPoolExecutor() as executor:
        future = executor.submit(BlockChain, 8888)
        blockchain = future.result()
    return blockchain


@pytest.fixture()
def transactions():
    return [
        Transaction('a', 'b', 100, 50, 'msg_1'),
        Transaction('c', 'd', 200, 20, 'msg_2')
    ]


def test_init_transaction():
    transaction = Transaction('sender', 'receiver', 10000, 50, 'test')
    assert transaction.sender
    assert transaction.receiver
    assert transaction.amounts
    assert transaction.fee
    assert transaction.message


def test_init_block(block):
    assert block.difficulty
    assert block.hash == ''
    assert block.miner
    assert block.miner_rewards
    assert block.nonce == 0
    assert block.previous_hash
    assert len(block.transactions) == 0
    assert type(block.timestamp) is int


def test_init_blockchain(blockchain):
    assert type(blockchain.adjust_difficulty_blocks) is int
    assert type(blockchain.block_limitation) is int
    assert type(blockchain.block_time) is int
    assert len(blockchain.chain) == 0
    assert type(blockchain.difficulty) is int
    assert type(blockchain.miner_rewards) is int
    assert len(blockchain.pending_transactions) == 0

    assert blockchain.connection_nodes == {}
    assert type(blockchain.node_address) is set
    assert blockchain.socket_host == '127.0.0.1'
    assert type(blockchain.socket_port) is int

    assert not blockchain.receive_verified_block


@handle_not_implemented_exception
def test_get_hash(blockchain, block, transactions):
    block.transactions = transactions
    assert blockchain.get_hash(block) == 'e9ba8e4af6ea0dfc92278fa32fcee949ee545b6a'


@handle_not_implemented_exception
def test_create_genesis_block(blockchain):
    blockchain.create_genesis_block()
    genesis_block = blockchain.chain[-1]
    assert type(genesis_block) is Block
    assert type(genesis_block.previous_hash) is str
    assert type(genesis_block.difficulty) is int
    assert type(genesis_block.miner) is str
    assert type(genesis_block.miner_rewards) is int
    assert type(genesis_block.timestamp) is int

    genesis_block_hash = blockchain.get_hash(genesis_block)
    assert genesis_block_hash == genesis_block.hash


@handle_not_implemented_exception
def test_add_transaction_to_block(blockchain, block, transactions):
    limitation = blockchain.block_limitation
    blockchain.pending_transactions = transactions * limitation
    origin_length = len(blockchain.pending_transactions)
    block = blockchain.add_transaction_to_block(block)
    assert len(block.transactions) == limitation
    assert len(blockchain.pending_transactions) == origin_length - limitation


@handle_not_implemented_exception
def test_add_transaction_to_block(blockchain, block, transactions):
    limitation = blockchain.block_limitation
    blockchain.pending_transactions = transactions * limitation
    origin_length = len(blockchain.pending_transactions)
    block = blockchain.add_transaction_to_block(block)
    assert len(block.transactions) == limitation
    assert len(blockchain.pending_transactions) == origin_length - limitation


@handle_not_implemented_exception
def test_mine_block(blockchain, transactions):
    blockchain.create_genesis_block()
    blockchain.pending_transactions = transactions
    blockchain.mine_block('cheese')

    new_block = blockchain.chain[-1]
    assert new_block.hash == blockchain.get_hash(new_block)


@handle_not_implemented_exception
def test_adjust_difficulty(blockchain):
    adjust_difficulty_blocks = 10
    for i in range(adjust_difficulty_blocks+1):
        blockchain.chain.append(Block('previous_hash', 5, 'cheese', 50, i+1))
    blockchain.adjust_difficulty_blocks = adjust_difficulty_blocks

    blockchain.block_time = 2
    blockchain.adjust_difficulty()
    assert blockchain.difficulty == 2

    blockchain.adjust_difficulty()
    assert blockchain.difficulty == 3

    blockchain.block_time = 0.5
    blockchain.adjust_difficulty()
    assert blockchain.difficulty == 2

    blockchain.adjust_difficulty()
    assert blockchain.difficulty == 1


@handle_not_implemented_exception
def test_get_balance(blockchain, transactions):
    for i in range(5):
        block = Block('previous_hash', 5, 'cheese', 50, i)
        block.transactions = transactions
        blockchain.chain.append(block)
    assert blockchain.get_balance('d') == 1000
    assert blockchain.get_balance('a') == -750
    assert blockchain.get_balance('cheese') == 600


@handle_not_implemented_exception
def test_verify_blockchain(blockchain):
    blockchain.create_genesis_block()
    blockchain.mine_block('cheese')
    assert blockchain.verify_blockchain()

    #fake_transaction = Transaction('fake_sender', 'fake_receiver', 10000, 10, 'msg')
    #blockchain.chain[1].transactions.append(fake_transaction)
    #assert blockchain.verify_blockchain()


@handle_not_implemented_exception
def test_mine_block_v2(blockchain, transactions):
    if 'MINE_BLOCK' not in os.environ or os.environ['MINE_BLOCK'] != 'v2':
        pytest.skip('Not Implemented')
    blockchain.create_genesis_block()
    blockchain.pending_transactions = transactions
    blockchain.receive_verified_block = True
    assert blockchain.mine_block('cheese') == False
    assert not blockchain.receive_verified_block
